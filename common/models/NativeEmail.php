<?php
/**
 * Created by PhpStorm.
 * User: stark
 * Date: 24.04.17
 * Time: 20:48
 */

namespace common\models;


class NativeEmail
{
    protected $email;
    protected $phone = '';

    /**
     * @param $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }


    public function sendNativeEmail()
    {
        $from = "Floris | Крымский чай и сладости floristea.com <info@floristea.com>";
        $to = 'info@floristea.com';
        $subject = 'Запрос прайса';
        $html = '';
        $html .= '<tr>Здравствуйте!<br/><br/></tr>';
        $html .= '<tr>Прошу выслать каталог товаров с актуальными ценами на email '.$this->email.'<br/><br/></tr>';
        if($this->phone != '') {
            $html .= '<tr>Мой номер телефона - '.$this->phone.'</tr>';
        }
        $headers  = "Content-type: text/html; charset=utf-8 \r\n";
        $headers .= 'From: '.$from."\r\n";
        return(mail($to, $subject, $html, $headers));

    }
}
